package com.bigshop;

import java.math.BigDecimal;
import java.math.MathContext;

public class Price {

    private static final int NO_TAX = 0;
    BigDecimal tax = new BigDecimal(0.12);
    MathContext precision = new MathContext(2);

    private BigDecimal totalAmount;
    private BigDecimal totalTaxAmount;
    private BigDecimal totalAmountWithTax;

    public void setTax() {
        this.tax = new Price().tax;
    }

    public void setTaxFree() {
        this.tax = new BigDecimal(0);
    }

    public BigDecimal getTax() {
        return tax;
    }

    private BigDecimal countTaxPrice(BigDecimal price) {
        /*if(tax == NO_TAX) {
            return NO_TAX;
        } else {
            return (price.multiply(tax));
        }*/
        return (price.multiply(tax, precision));
    }

    public BigDecimal countPriceTotal(BigDecimal price) {
        return price.add(countTaxPrice(price));
    }

    public BigDecimal countPriceTotal(BigDecimal price, int quantity) {
        return price.multiply(new BigDecimal(quantity), precision);
    }

    public BigDecimal countTaxTotal(BigDecimal price, int quantity) {
        return countTaxPrice(price).multiply(new BigDecimal(quantity), precision);
    }

    public BigDecimal countPriceTotalWithTax(BigDecimal price, int quantity) {
        return countPriceTotal(price).multiply(new BigDecimal(quantity), precision);
    }

    public synchronized void setTotalAmountAndTax(BigDecimal price, int quantity) {
        setTotalAmount(countPriceTotal(price, quantity));
        setTotalTaxAmount(countTaxTotal(price, quantity));
        setTotalAmountWithTax(countPriceTotalWithTax(price, quantity));
    }

    public synchronized void setTotalAmount(BigDecimal amount) {
        totalAmount.add(amount);
    }

    public synchronized void setTotalTaxAmount(BigDecimal tax) {
        totalTaxAmount.add(tax);
    }

    public synchronized void setTotalAmountWithTax(BigDecimal amount) {
        totalAmountWithTax.add(amount);
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public BigDecimal getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public BigDecimal getTotalAmountWithTax() {
        return totalAmountWithTax;
    }
}
