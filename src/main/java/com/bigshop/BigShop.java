package com.bigshop;

import com.bigshop.entity.Address;
import com.bigshop.entity.Item;

import java.util.*;
import java.util.stream.Stream;

public class BigShop {

    private static final String SIM_CARD = Item.SIM_CARD.getItemName();
    private static final String PHONE_CASE = Item.PHONE_CASE.getItemName();
    private static final String PHONE_INSURANCE = Item.PHONE_INSURANCE.getItemName();
    private static final String WIRED_EARPHONE = Item.WIRED_EARPHONE.getItemName();
    private static final String WIRELESS_EARPHONE = Item.WIRELESS_EARPHONE.getItemName();

    public static void main(String[] args) {

        HelperMethod hlpMet = new HelperMethod();
        Map<String, Integer> map = new LinkedHashMap<>();

        String[] shoppingCart = new String[] {SIM_CARD, SIM_CARD, SIM_CARD, PHONE_CASE, WIRELESS_EARPHONE};
        //String[] shoppingCart = new String[] {SIM_CARD, SIM_CARD, PHONE_CASE, SIM_CARD, SIM_CARD, SIM_CARD, SIM_CARD, SIM_CARD, SIM_CARD, SIM_CARD, WIRED_EARPHONE, SIM_CARD, SIM_CARD};
        //String[] shoppingCart = new String[] {};
        //String[] shoppingCart = new String[] {PHONE_CASE, "sim card", "1", "0", SIM_CARD};
        List<String> shoppingList = new ArrayList<>(Arrays.asList(shoppingCart));

        if(hlpMet.checkInputItems(shoppingList)) {
            hlpMet.addShoppingListIntoMap(shoppingList, map);
        }

        map.forEach((key, value) -> System.out.println(key + ": " + value));


        /*Stream.of(map.keySet().toString(), map.values().toString())
                .forEach(System.out::println);
*/
        new PrintReceipt().printReceipt(map);

    }
}
