package com.bigshop.entity;

import com.bigshop.entity.Item;

import java.math.BigDecimal;
import java.math.MathContext;

public class PhoneInsurance {

    BigDecimal price = Item.PHONE_INSURANCE.getPrice();
    BigDecimal discount = new BigDecimal(0.2);

    public BigDecimal getDiscount() {
        return price.subtract(price.multiply(discount, new MathContext(2)));
    }
}
