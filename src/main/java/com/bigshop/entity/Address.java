package com.bigshop.entity;

import java.util.stream.Stream;

public class Address {

    private static final String SHOP_NAME = "'BIG' SHOP";
    private static final String STREET = "Gotthardstrasse 6";
    private static final String CITY = "Zürich";
    private static final String POSTCODE = "239 94";
    private static final String PHONE = "Phone number: +430 849 322 944";
    private static final String WEB = "Website: www.big-shop.chf";

    public void getAddress() {
        Stream.of(SHOP_NAME, STREET, POSTCODE + " " + CITY, PHONE, WEB)
                .forEach(System.out::println);
    }
}
