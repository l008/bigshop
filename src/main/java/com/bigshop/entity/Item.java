package com.bigshop.entity;

import java.math.BigDecimal;
import java.util.*;

public enum Item{

    SIM_CARD ("SIM card", new BigDecimal(20), new BigDecimal(0.12), 10),
    PHONE_CASE ("Phone case", new BigDecimal(10), new BigDecimal(0.12)),
    PHONE_INSURANCE ("Phone insurance", new BigDecimal(120), new BigDecimal(0), new BigDecimal(0.2)),
    WIRED_EARPHONE ("Wired earphone", new BigDecimal(30), new BigDecimal(0.12)),
    WIRELESS_EARPHONE ("Wireless earphone", new BigDecimal(50), new BigDecimal(0.12));

    //SIM_CARD_FREE ("Free SIM card - BOGOF", new BigDecimal(0), new BigDecimal(0.12)),
    //PHONE_INSURANCE_DISCOUNT ("Phone insurance - DISCOUNT 20%", new PhoneInsurance().getDiscount(), new BigDecimal(0)),

    private final String itemName;
    private BigDecimal price;
    private BigDecimal tax;
    private BigDecimal discount;
    private int maxCount;

    Item(String itemName, BigDecimal price, BigDecimal tax) {
        this.itemName = itemName;
        this.price = price;
        this.tax = tax;
    }

    Item(String itemName, BigDecimal price, BigDecimal tax, BigDecimal discount) {
        this.itemName = itemName;
        this.price = price;
        this.tax = tax;
        this.discount = discount;
    }

    Item(String itemName, BigDecimal price, BigDecimal tax, int maxCount) {
        this.itemName = itemName;
        this.price = price;
        this.tax = tax;
        this.maxCount = maxCount;
    }

    public String getItemName() {
        return itemName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public static Item getEnumName(String item) {
        return Arrays.stream(Item.values())
                .filter(value -> value.itemName.equals(item))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown item: " + item));
    }
}
