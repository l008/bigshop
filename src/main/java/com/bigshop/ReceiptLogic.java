package com.bigshop;

import com.bigshop.entity.Item;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class ReceiptLogic {

    private static final DecimalFormat formatPrice = new DecimalFormat("0.00");
    private static final DecimalFormat formatPercent = new DecimalFormat("0%");

    private static Price price = new Price();
    private static boolean discountFlag = false;

    public String getActualDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        return sdf.format(new Date());
    }

    public String generateNumberOfReceipt() {
        Calendar cal = Calendar.getInstance();
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH));
        String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
        String hour = String.valueOf(cal.get(Calendar.HOUR));
        String second = String.valueOf(cal.get(Calendar.SECOND));

        return String.join("", year, month, day, hour, second);
    }

    private void itemDetail(Item itemName, int quantity) {
        BigDecimal totalPrice = price.countPriceTotal(itemName.getPrice(), quantity);

        System.out.println(itemName.getItemName());
        System.out.println(formatPercent.format(price.getTax()) + "        " + quantity + " x " + itemName.getPrice() + "               "
                + formatPrice.format(totalPrice));
    }

    public void printItemDetails(Map<String, Integer> map) {
        for (String key : map.keySet()) {
            int quantity = map.get(key).intValue();
            Item item = Item.getEnumName(key);

            if (Item.PHONE_INSURANCE.getItemName().equals(key)) {
                BigDecimal tax = Item.PHONE_INSURANCE.getTax();
            }

            if (Item.SIM_CARD.getItemName().equals(key)) {
                if (new ItemLogic().checkFreeSimCards(quantity)) {
                    int freeSimCards = new ItemLogic().countFreeSimCards(quantity);

                    itemDetail(Item.SIM_CARD, freeSimCards);
                    quantity -= freeSimCards;
                }
            }

            if (Item.PHONE_INSURANCE.getItemName().equals(key)) {
                item.getDiscount();
            }

            itemDetail(item, quantity);
            price.setTotalAmountAndTax(item.getPrice(), quantity);
        }
    }

    public void printItemDetails(int countItem, Item itemName) {
        if(countItem > 0) {
            if(Item.PHONE_INSURANCE.equals(itemName)) {
                price.setTaxFree();
            } else {
                price.setTax();
            }

            /*if(Item.SIM_CARD.equals(itemName)) {
                SimCard sim = new SimCard();

                if(sim.checkFreeSimCards(countItem)) {
                    int freeSimCards = sim.countFreeSimCards(countItem);

                    itemDetail(freeSimCards, Item.SIM_CARD_FREE);
                    countItem -= freeSimCards;
                }
            }
            if(Item.PHONE_INSURANCE.equals(itemName) && discountFlag) {
                itemName = Item.PHONE_INSURANCE_DISCOUNT;
            }*/

            itemDetail(itemName, countItem);
            price.setTotalAmountAndTax(itemName.getPrice(), countItem);
        }
    }
}
