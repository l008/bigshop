package com.bigshop;

import com.bigshop.entity.Item;

import java.math.BigDecimal;
import java.math.MathContext;

public class ItemLogic {

    /*public boolean checkMaxCount(int countSimCards) {
        if(countSimCards > Item.SIM_CARD.getMaxCount()) {
            System.out.println("Can't buy more than 10 sim cards in a single purchase!");
            return true;
        }
        return false;
    }*/

    public boolean checkFreeSimCards(int countSimCards) {
        return (countSimCards > 2);
    }

    public int countFreeSimCards(int countSimCards) {
        return countSimCards / 2;
    }

    public BigDecimal getDiscount(BigDecimal price, BigDecimal discount) {
        return price.subtract(price.multiply(discount, new MathContext(2)));
    }

    public boolean checkEarphoneInReceipt(int countWiredEarphone, int countWirelessEarphone) {
        return (countWiredEarphone > 0 || countWirelessEarphone > 0);
    }
}
