package com.bigshop;

import com.bigshop.entity.Address;
import com.bigshop.entity.Item;

import java.util.Map;

public class PrintReceipt {

    ReceiptLogic receiptLogic = new ReceiptLogic();
    Receipt receipt = new Receipt();
    Price price = new Price();

    public void printReceipt(Map<String, Integer> map) {
        receipt.printHeader(receiptLogic.getActualDateTime(), receiptLogic.generateNumberOfReceipt());

        receiptLogic.printItemDetails(map);

        //receiptLogic.printItemDetails(countSimCards, Item.SIM_CARD);
        //receiptLogic.printItemDetails(countPhoneCases, Item.PHONE_CASE);
        //receiptLogic.printItemDetails(countPhoneInsurance, Item.PHONE_INSURANCE);
        //receiptLogic.printItemDetails(countWiredEarphone, Item.WIRED_EARPHONE);
        //receiptLogic.printItemDetails(countWirelessEarphone, Item.WIRELESS_EARPHONE);

        receipt.printFooter(price);
    }
}
