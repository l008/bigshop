package com.bigshop;

import com.bigshop.entity.Address;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Receipt {

    private static final DecimalFormat formatPrice = new DecimalFormat("0.00");
    private static final DecimalFormat formatPercent = new DecimalFormat("0%");

    ReceiptLogic receipt = new ReceiptLogic();
    private static Price price;

    private static boolean discountFlag = false;

    private void printContactHeader() {
        new Address().getAddress();

        System.out.println("\nDate of taxable supply: " + receipt.getActualDateTime());
        System.out.println("Simplified tax receipt: " + receipt.generateNumberOfReceipt());
    }

    private void printContactHeader(String actualDateTime, String receiptNumber) {
        new Address().getAddress();

        System.out.println("\nDate of taxable supply: " + actualDateTime);
        System.out.println("Simplified tax receipt: " + receiptNumber);
    }

    private void printItemFooter(Price price) {
        System.out.println("-------------------------------------------");
        System.out.println("Tax base                         " + formatPrice.format(price.getTotalAmount()));
        System.out.println("Tax " + formatPercent.format(new Price().tax) + "                           " + formatPrice.format(price.getTotalTaxAmount()));
        System.out.println("Total amount (CHF)               " + formatPrice.format(price.getTotalAmountWithTax()));

        System.out.println("\nCash register number: 08");
        System.out.println("Sold by: Antonio Bruddas");
    }

    private void printItemHeader() {
        System.out.println("\nItem name");
        System.out.println("Tax %        Amount             Price (CHF)");
        System.out.println("-------------------------------------------");
    }

    private void printItemFooter() {
        System.out.println("-------------------------------------------");
        System.out.println("Tax base                         " + formatPrice.format(price.getTotalAmount()));
        System.out.println("Tax " + formatPercent.format(new Price().tax) + "                           " + formatPrice.format(price.getTotalTaxAmount()));
        System.out.println("Total amount (CHF)               " + formatPrice.format(price.getTotalAmountWithTax()));

        System.out.println("\nCash register number: 08");
        System.out.println("Sold by: Antonio Bruddas");
    }

    public void printHeader(String actualDateTime, String receiptNumber) {
        printContactHeader(actualDateTime, receiptNumber);
        printItemHeader();
    }

    public void printFooter(Price price) {
        printItemFooter(price);
    }

    public void printHeader() {
        printContactHeader();
        printItemHeader();
    }

    public void printFooter() {
        printItemFooter();
    }
}
