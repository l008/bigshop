package com.bigshop;

import com.bigshop.entity.Item;

import java.util.*;
import java.util.stream.Stream;

public class HelperMethod {

    public boolean checkInputItems(List<String> shoppingList) {
        List<String> itemsList = new ArrayList<>();
        Stream.of(Item.values()).forEach(item -> itemsList.add(item.getItemName()));

        int counterSimCards = 0;

        if (shoppingList.isEmpty() || shoppingList == null) {
            System.out.println("Shopping list is empty!");
            return false;
        }

        for (String item : shoppingList) {
            if (!itemsList.contains(item)) {
                System.out.println("Wrong item in the shopping list!");
                return false;
            }
            if (Item.SIM_CARD.getItemName().equals(item)) {
                counterSimCards++;
            }
        }

        if(counterSimCards > 10) {
            System.out.println("Can't buy more than 10 SIM cards!");
            return false;
        }

        return true;
    }

    public static void addShoppingListIntoMap(List shoppingList, Map<String, Integer> map) {
        List<String> itemList = new ArrayList<>(shoppingList);

        for (String item : itemList) {
            if (map.containsKey(item)) {
                int quantity = map.get(item).intValue();
                map.replace(item, quantity + 1);
            } else {
                map.put(item, 1);
            }
        }
    }

    public static int getItemCount(Map<String, Integer> map, Item itemName) {
        return map.get(itemName.getItemName()).intValue();
    }
}
