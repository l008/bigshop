package com.bigshop;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class PriceTest {

    private Price price;

    @Before
    void setUp() {
        price = new Price();
    }

    @After
    void tearDown() {
    }

    @Test
    void countPriceTotal() {
        assertEquals(22.4, price.countPriceTotal(new BigDecimal(10)));

        price.setTaxFree();
        assertEquals(120, price.countPriceTotal(new BigDecimal(120), 1));
    }

    @Test
    void testCountPriceTotal() {
        assertEquals(40, price.countPriceTotal(new BigDecimal(10), 2));
    }

    @Test
    void countTaxTotal() {
        price.setTaxFree();
        assertEquals(0, price.countTaxTotal(new BigDecimal(120), 1));

        price.setTax();
        assertEquals(24, price.countTaxTotal(new BigDecimal(50), 4));
    }

    @Test
    void countPriceTotalWithTax() {
        assertEquals(179.2, price.countPriceTotalWithTax(new BigDecimal(10), 8));

        price.setTaxFree();
        assertEquals(96, price.countPriceTotalWithTax(new BigDecimal(96), 1));
    }
}